# TP_2_enigme_quantique_2

# liens vidéo pour vidéos TP_enigme_quantique_N°2

- voir dossier 'ressources' avec vidéo explicative.

# objectifs du TP :

- explications détaillées sur la résolution de l'énigme + déroulement du code;
- générations de visualisation diverses des résultats de l'énigme et des différentes possibilités : états et mesures des Qubits.

# date de rendu :

- TP à rendre pour le 08/12/2023;